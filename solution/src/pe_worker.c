/// @file
/// @brief PE worker application file

/// standard valid value for e_magic of the image dos header
#define E_MAGIC_CONSTANT_x86 0x5a4d
/// standard valid value for start of the PE File data
#define PE_SIGNATURE 0x4550

#include <pe_worker.h>


IMAGE_SECTION_HEADER *read_pe_file(FILE *in_file, char section_name[IMAGE_SIZE_OF_SHORT_NAME], PEFile *peFile) {
    IMAGE_DOS_HEADER dos_header = {0};
    fread(&dos_header.e_magic, sizeof(WORD), 1, in_file);
    fprintf(stdout, "INFO: current e_magic:%d\n", dos_header.e_magic);
    if (dos_header.e_magic != E_MAGIC_CONSTANT_x86) {
        fprintf(stderr, "Wrong e_magic\n");
        return NULL;
    }
    fseek(in_file, peFile->offset_to_e_lfanew, SEEK_SET);
    LONG tmp = 0;
    fread(&tmp, sizeof(LONG), 1, in_file);
    fprintf(stdout, "INFO: current offset to e_lfanew:%d\n", peFile->offset_to_e_lfanew);
//    fread(&dos_header.e_lfanew, sizeof(LONG), 1, in_file);
    // Заголовок PE файла должен быть выровнен по DWORD
    dos_header.e_lfanew = tmp;
    fprintf(stdout, "INFO: current e_lfanew:%d\n", dos_header.e_lfanew);
    if (dos_header.e_lfanew % sizeof(DWORD) != 0) {
        fprintf(stderr, "Wrong e_lfanew\n");
        return NULL;
    }
    fseek(in_file, dos_header.e_lfanew, SEEK_SET);
    IMAGE_NT_HEADERS64 imageNtHeaders64 = {0};
    fread(&imageNtHeaders64.Signature, sizeof(DWORD), 1, in_file);
    //Check is signature valid or not
    fprintf(stdout, "INFO: current signature:%u\n", imageNtHeaders64.Signature);
    if (imageNtHeaders64.Signature != PE_SIGNATURE) {
        fprintf(stderr, "Wrong signature\n");
        return NULL;
    }
    fseek(in_file, sizeof(WORD), SEEK_CUR);
    fread(&imageNtHeaders64.FileHeader.NumberOfSections, sizeof(WORD), 1, in_file);
    fseek(in_file, peFile->size_of_file_header, SEEK_CUR);
    WORD optionalHeaderMagic = {0};
    fread(&optionalHeaderMagic, sizeof(WORD), 1, in_file);
    LONG first_section_ptr = dos_header.e_lfanew;
    fprintf(stdout, "INFO: current optional header magic:%hu\n", optionalHeaderMagic);
    if (optionalHeaderMagic == 0x20B) {
        first_section_ptr += peFile->size_of_image_optional_header64;
    }
    if (optionalHeaderMagic == 0x10B) {
        first_section_ptr += peFile->size_of_image_optional_header32;
    }
    fprintf(stdout, "INFO: first section ptr:%d\n", first_section_ptr);
    IMAGE_SECTION_HEADER *sectionHeader = malloc(sizeof(IMAGE_SECTION_HEADER));
    fseek(in_file, first_section_ptr, SEEK_SET);
    for (WORD i = 0; i < imageNtHeaders64.FileHeader.NumberOfSections; i++) {
        fread(sectionHeader->Name, sizeof(sectionHeader->Name), 1, in_file);
        fseek(in_file, sizeof(DWORD) * 2, SEEK_CUR);
        fread(&sectionHeader->SizeOfRawData, sizeof(sectionHeader->SizeOfRawData), 1, in_file);
        fread(&sectionHeader->PointerToRawData, sizeof(sectionHeader->PointerToRawData), 1, in_file);
        if (memcmp(section_name, sectionHeader->Name, 6) == 0) {
            return sectionHeader;
        }
        fseek(in_file, sizeof(DWORD) * 3 + sizeof(WORD) * 2, SEEK_CUR);
    }
    free(sectionHeader);
    fprintf(stderr, "Section not found\n");
    return NULL;
}

void write_output_data(FILE *in_file, FILE *out_file, IMAGE_SECTION_HEADER *sectionHeader) {
    fseek(in_file, sectionHeader->PointerToRawData, SEEK_SET);
    char *buff = malloc(sectionHeader->SizeOfRawData);
    fread(buff, 1, sectionHeader->SizeOfRawData, in_file);
    fwrite(buff, 1, sectionHeader->SizeOfRawData, out_file);
    free(buff);
}
