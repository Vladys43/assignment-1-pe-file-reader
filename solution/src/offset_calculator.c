/// @file
/// @brief File for auto calculation offsets
#include <offset_calculator.h>

/// Size of file header the header of PE file
#define SIZE_OF_FILE_HEADER 20
/// Size of optional header in 32 bits machines of the header of PE file
#define SIZE_OF_OPTIONAL_HEADER32 248
/// Size of optional header in 64 bits machines of the header of PE file
#define SIZE_OF_OPTIONAL_HEADER64 264


void calculate_offset_of_PEFile(PEFile *file) {
    file->offset_to_e_lfanew = 60;
    file->size_of_file_header = SIZE_OF_FILE_HEADER - sizeof(WORD)*2;
    file->size_of_image_optional_header32 = SIZE_OF_OPTIONAL_HEADER32;
    file->size_of_image_optional_header64 = SIZE_OF_OPTIONAL_HEADER64;
}
