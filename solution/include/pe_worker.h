///@headerfile
///@brief header file for pe worker
#include <malloc.h>
#include <pe_data.h>
#include <stdio.h>
#include <string.h>

/// @brief Read the data from PE file
/// @param[in] in_file_name Name of the file to open and read
/// @param[in] section_name Name of the section we needs
/// @return The valid Header of necessary section
IMAGE_SECTION_HEADER *read_pe_file(FILE *in_file, char section_name[IMAGE_SIZE_OF_SHORT_NAME], PEFile *peFile);

/// @brief Write data of desired section
/// @param[in] in_file Input file which contains data
/// @param[in] out_file Output file to write data which readed from Input file
/// @param[in] sectionHeader Information about section which need to write
void write_output_data(FILE *in_file, FILE *out_file, IMAGE_SECTION_HEADER *sectionHeader);
