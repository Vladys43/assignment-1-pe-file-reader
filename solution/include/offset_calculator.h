///@headerfile
///@brief header file for offset calculator
#ifndef ASSIGNMENT_1_PE_FILE_READER_OFFSET_CALCULATOR_H
#define ASSIGNMENT_1_PE_FILE_READER_OFFSET_CALCULATOR_H

#include <pe_data.h>

/// @brief Load into the struct data about sizes and offsets
/// @param[in] file Struct to save the data
void calculate_offset_of_PEFile(PEFile* file);

#endif //ASSIGNMENT_1_PE_FILE_READER_OFFSET_CALCULATOR_H
